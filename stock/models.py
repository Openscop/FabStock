from django.db import models
from django.utils import timezone

# Create your models here.


"""
Modèle Stock
"""


class Stock(models.Model):
    date_entree = models.DateTimeField(default=timezone.now())
    categorie = models.ForeignKey('Categorie', blank=True, null=True)
    type = models.ForeignKey('Type', blank=True, null=True)
    acces = models.ForeignKey('Acces', blank=True, null=True)
    quantite = models.IntegerField()
    designation = models.CharField(max_length=200, blank=True)
    marque = models.CharField(max_length=20, blank=True)
    modele = models.CharField(max_length=20, blank=True)
    prix_vente = models.FloatField(blank=True, null=True)
    etat = models.CharField(max_length=20, blank=True)
    proprietaire = models.ForeignKey('Proprietaire', blank=True, null=True)
    date_inventaire = models.DateTimeField(blank=True, null=True)
    commentaire = models.CharField(max_length=200, blank=True)
    prix_achat = models.FloatField(blank=True, null=True)
    url_fournisseur = models.URLField(blank=True)

    def __str__(self):
        return self.designation


"""
Modèle Categorie
l'attribut couleur servira à les différencier à l'affichage
"""


class Categorie(models.Model):
    libelle = models.CharField(max_length=20)
    couleur = models.CharField(max_length=7, default='#ffffff')

    def __str__(self):
        return self.libelle


"""
Modèle Type
l'attribut couleur servira à les différencier à l'affichage
"""


class Type(models.Model):
    libelle = models.CharField(max_length=20)
    couleur = models.CharField(max_length=7, default='#ffffff')

    def __str__(self):
        return self.libelle


"""
Modèle Proprietaire
l'attribut couleur servira à les différencier à l'affichage
"""


class Proprietaire(models.Model):
    libelle = models.CharField(max_length=20)
    couleur = models.CharField(max_length=7, default='#ffffff')

    def __str__(self):
        return self.libelle


"""
Modèle Acces
l'attribut couleur servira à les différencier à l'affichage
"""


class Acces(models.Model):
    libelle = models.CharField(max_length=20)
    couleur = models.CharField(max_length=7, default='#ffffff')

    def __str__(self):
        return self.libelle
